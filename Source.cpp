#include <iostream>

using namespace std;

struct MyStruct 
{
	MyStruct() 
	{

	}

	MyStruct(string f1, string f2)
	{
		field1 = f1;
		field2 = f2;
	}

	string getValueByFieldName(string fieldName)
	{
		if (fieldName == "field1")
			return field1;
		else if (fieldName == "field2")
			return field2;
	}

	string field1;
	string field2;
};

bool isGreater(MyStruct o1, MyStruct o2, string fieldName)
{
	return o1.getValueByFieldName(fieldName) > o2.getValueByFieldName(fieldName);
}

void sort(MyStruct* arr, int n, string fieldName)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n; j++)
		{
			if (isGreater(arr[i], arr[j], fieldName))
			{
				MyStruct tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
		}
	}
}

void printArr(MyStruct* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "field1 = " << arr[i].field1 << ", field2 = " << arr[i].field2 << endl;
	}
	cout << endl;
}

int main()
{
	int n = 3;
	MyStruct* arr = new MyStruct[n];
	arr[0] = MyStruct("2", "2");
	arr[1] = MyStruct("3", "1");
	arr[2] = MyStruct("1", "3");
	cout << "Array before sorting:" << endl;
	printArr(arr, n);

	cout << "Array after sorting by field1:" << endl;
	sort(arr, n, "field1");
	printArr(arr, n);

	cout << "Array after sorting by field2:" << endl;
	sort(arr, n, "field2");
	printArr(arr, n);

	return 0;
}